class Post < ActiveRecord::Base
    has_many :comments, dependent: :destroy
    validates :title, presence: true
    validates :title, length: { in: 5..30, 
        message: "Titulo invalido"}
    validates :body, presence: true
    validates :body, length: { maximum: 50,
        message: "El texto del post es demasiado largo"}
    validates :body, length: { minimum: 10,
        message: "El texto del post demasiado corto"}
    
end
